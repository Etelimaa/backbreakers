// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerArcadeInputController.h"
#include "PhysicsEngine/PhysicsThrusterComponent.h"
#include "Components/AudioComponent.h"
#include "ShipWeaponController.h"
#include "Weapon_AC.h"
#include "PlayerShip.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UPlayerArcadeInputController::UPlayerArcadeInputController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UPlayerArcadeInputController::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
	SetInputComponent();
	SetThrusters();
	SetWeaponController();
	Owner = Cast<APlayerShip>(GetOwner());
}


// Called every frame
void UPlayerArcadeInputController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlayerArcadeInputController::SetComponentDamage(UPhysicsThrusterComponent * ThrusterHit, bool IsIncapacitated, bool IsStuckActive)
{
}

void UPlayerArcadeInputController::Pitch(float AxisValue)
{
}

void UPlayerArcadeInputController::Roll(float AxisValue)
{
}

void UPlayerArcadeInputController::Yaw(float AxisValue)
{
}

void UPlayerArcadeInputController::Hover(float AxisValue)
{
}

void UPlayerArcadeInputController::Thrust(float AxisValue)
{
}

void UPlayerArcadeInputController::Fire()
{
}

void UPlayerArcadeInputController::SetInputComponent()
{
}

void UPlayerArcadeInputController::SetThrusters()
{
}

void UPlayerArcadeInputController::SetWeaponController()
{
}

