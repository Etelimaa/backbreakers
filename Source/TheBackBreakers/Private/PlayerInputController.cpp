// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerInputController.h"
#include "PhysicsEngine/PhysicsThrusterComponent.h"
#include "Components/AudioComponent.h"
#include "ShipWeaponController.h"
#include "Weapon_AC.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PlayerShip.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UPlayerInputController::UPlayerInputController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerInputController::BeginPlay()
{
	Super::BeginPlay();

	// ...
	SetInputComponent();
	SetThrusters();
	SetWeaponController();
	Owner = Cast<APlayerShip>(GetOwner());
}


// Called every frame
void UPlayerInputController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	CompileControlValues();

	CalculateSpeed();
}

// TODO Broadcasts bug in arcade mode!
void UPlayerInputController::Pitch(float AxisValue)
{
	if (AxisValue - 0.05f > 0.5f) {

		if (bThrusterBased) {
			NoseUp->ThrustStrength = PitchThrusterStrength * ((AxisValue - 0.5f) * 2.f);
			NoseUpAft->ThrustStrength = PitchThrusterStrength * ((AxisValue - 0.5f) * 2.f);
			NoseDown->ThrustStrength = 0.f;
			NoseDownAft->ThrustStrength = 0.f;
			PitchUp.Broadcast(((AxisValue - 0.5f) * 2.f));
		} else {
			PitchUp.Broadcast(((AxisValue - 0.5f) * 2.f));
			FRotator CurrentRotation;
			CurrentRotation.Pitch = AxisValue * 0.5f * -1.f;
			//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Pitch);
			PitchValue = CurrentRotation.Pitch;
			
		}
	}
	else if (AxisValue + 0.05f < 0.5f) {
		if (bThrusterBased) {
			NoseDown->ThrustStrength = PitchThrusterStrength * (((AxisValue - 0.5)  * 2.f) * -1.f);
			NoseDownAft->ThrustStrength = PitchThrusterStrength * (((AxisValue - 0.5)  * 2.f) * -1.f);
			NoseUp->ThrustStrength = 0.f;
			NoseUpAft->ThrustStrength = 0.f;
			PitchDown.Broadcast((((AxisValue - 0.5)  * 2.f) * -1.f));
		} else {
			PitchDown.Broadcast((((AxisValue - 0.5)  * 2.f) * -1.f));
			AxisValue = (AxisValue - 0.5f) * 2.f;
			FRotator CurrentRotation;
			CurrentRotation.Pitch = AxisValue * 0.5f * -1.f;
			//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Pitch);
			PitchValue = CurrentRotation.Pitch;
			
		}
	}
	else {
		if (bThrusterBased) {
			NoseUp->ThrustStrength = 0.f;
			NoseUpAft->ThrustStrength = 0.f;
			NoseDown->ThrustStrength = 0.f;
			NoseDownAft->ThrustStrength = 0.f;
			PitchNeutral.Broadcast(AxisValue);
		}else { 
			PitchNeutral.Broadcast(AxisValue);
			PitchValue = 0.f;
		}
	}
}

void UPlayerInputController::Roll(float AxisValue)
{
	UE_LOG(LogTemp, Warning, TEXT("Rolling: %f"), AxisValue);
	if (bThrusterBased) {

		if (bRollLeftIsStuckActive) {
			RollLeft->ThrustStrength = RollThrusterStrength;
			RollToLeft.Broadcast(1.f);
		}
		if (bRollLeftUpperIsStuckActive) {
			RollLeftUpper->ThrustStrength = RollThrusterStrength;
			RollToLeft.Broadcast(1.f);
		}
		if (bRollRightIsStuckActive) {
			RollRight->ThrustStrength = RollThrusterStrength;
			RollToRight.Broadcast(1.f);
		}
		if (bRollRightUpperIsStuckActive) {
			RollRightUpper->ThrustStrength = RollThrusterStrength;
			RollToRight.Broadcast(1.f);
		}

	}

	if (AxisValue - 0.05f > 0.5f) {
		float ThrusterModifier = ((AxisValue - 0.5f) * 2.f);
		if (!bRollRightIsIncapacitated && !bRollRightIsStuckActive) {

			if (bThrusterBased) {

				RollRight->ThrustStrength = RollThrusterStrength * ThrusterModifier;
				RollToRight.Broadcast(ThrusterModifier);
			} else {
				RollToRight.Broadcast(ThrusterModifier);
				FRotator CurrentRotation;
				CurrentRotation.Roll = AxisValue * 0.5f * -1.f;
				//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Roll);
				RollValue = CurrentRotation.Roll;

			}
		}

		if (!bRollRightUpperIsIncapacitated && !bRollRightUpperIsStuckActive) {

			if (bThrusterBased) {

				RollRightUpper->ThrustStrength = RollThrusterStrength * ThrusterModifier;
				RollToRight.Broadcast(ThrusterModifier);
			} else {}
		}

		// TODO Check if correct!
		if(!bRollLeftIsStuckActive)
			if (bThrusterBased) {
				RollLeft->ThrustStrength = 0.f;
			}else {}

		if(!bRollLeftUpperIsStuckActive)
			if (bThrusterBased) {
				RollLeftUpper->ThrustStrength = 0.f;
			}else {}
	}
	else if (AxisValue + 0.05f < 0.5f) {
		float ThrusterModifier = (((AxisValue - 0.5)  * 2.f) * -1.f);
		if (!bRollLeftIsIncapacitated && !bRollLeftIsStuckActive) {

			if (bThrusterBased) {

				RollLeft->ThrustStrength = RollThrusterStrength * ThrusterModifier;
				RollToLeft.Broadcast(ThrusterModifier);

			}else {
				RollToLeft.Broadcast(ThrusterModifier);
				AxisValue = (AxisValue - 0.5f) * 2.f;
				FRotator CurrentRotation;
				CurrentRotation.Roll = AxisValue * 0.5f * -1.f;
				//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Roll);
				RollValue = CurrentRotation.Roll;

			}
		}
		if (!bRollLeftUpperIsIncapacitated && !bRollLeftUpperIsStuckActive) {

			if (bThrusterBased) {

				RollLeftUpper->ThrustStrength = RollThrusterStrength * ThrusterModifier;
				RollToLeft.Broadcast(ThrusterModifier);

			}else {}
		}

		if(!bRollRightIsStuckActive)
			if (bThrusterBased) {
				RollRight->ThrustStrength = 0.f;
			}else {}

		if(!bRollRightUpperIsStuckActive)
			if (bThrusterBased) {
				RollRightUpper->ThrustStrength = 0.f;
			}else {}
	}
	else {
		if(!bRollLeftIsStuckActive)
			if (bThrusterBased) {
				RollLeft->ThrustStrength = 0.f;
			}else {}

		if(!bRollLeftUpperIsStuckActive)
			if (bThrusterBased) {
				RollLeftUpper->ThrustStrength = 0.f;
			}else {}

		if(!bRollRightIsStuckActive)
			if (bThrusterBased) {
				RollRight->ThrustStrength = 0.f;
			}else {}

		if(!bRollRightUpperIsStuckActive)
			if (bThrusterBased) {
				RollRightUpper->ThrustStrength = 0.f;
			}else {}

		if (!bRollLeftIsStuckActive && !bRollLeftUpperIsStuckActive && !bRollRightIsStuckActive && !bRollRightUpperIsStuckActive) {
			RollNeutral.Broadcast(AxisValue);
			RollValue = 0.f;
		}
	}
}

void UPlayerInputController::Yaw(float AxisValue)
{
	if (AxisValue - 0.05f > 0.5f) {

		if (bThrusterBased) {

			YawRight->ThrustStrength = RollThrusterStrength * ((AxisValue - 0.5f) * 2.f);
			YawLeft->ThrustStrength = 0.f;
			YawToRight.Broadcast(((AxisValue - 0.5f) * 2.f));

		}else {
			YawToRight.Broadcast(((AxisValue - 0.5f) * 2.f));
			FRotator CurrentRotation;
			CurrentRotation.Yaw = AxisValue * 0.5f * 1.f;
			//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Yaw);
			YawValue = CurrentRotation.Yaw;
		}
	}
	else if (AxisValue + 0.05f < 0.5f) {

		if (bThrusterBased) {

			YawLeft->ThrustStrength = RollThrusterStrength * (((AxisValue - 0.5)  * 2.f) * -1.f);
			YawRight->ThrustStrength = 0.f;
			YawToLeft.Broadcast((((AxisValue - 0.5)  * 2.f) * -1.f));

		}else {
			YawToLeft.Broadcast((((AxisValue - 0.5)  * 2.f) * -1.f));
			AxisValue = (AxisValue - 0.5f) * 2.f;
			FRotator CurrentRotation;
			CurrentRotation.Yaw = AxisValue * 0.5f * 1.f;
			//UE_LOG(LogTemp, Error, TEXT("%f"), CurrentRotation.Yaw);
			YawValue = CurrentRotation.Yaw;
		}
	}
	else {

		if (bThrusterBased) {
			YawLeft->ThrustStrength = 0.f;
			YawRight->ThrustStrength = 0.f;
			YawNeutral.Broadcast(AxisValue);
		}else {
			YawNeutral.Broadcast(AxisValue);
			YawValue = 0.f;
		}
	}
}

void UPlayerInputController::Hover(float AxisValue)
{
	FString str = FString::SanitizeFloat(AxisValue);


	if (bHoverLeftIsStuckActive) {
		if (bThrusterBased) {
			HoverLeft->ThrustStrength = HoverThrusterStrength;
		}else{}
		HoverToLeft.Broadcast(1.f);
	}
	if (bHoverRightIsStuckActive) {
		if (bThrusterBased) {
			HoverRight->ThrustStrength = HoverThrusterStrength;
		}
		HoverToRight.Broadcast(1.f);
	}
	if (bHoverUpIsStuckActive) {
		if (bThrusterBased) {
			HoverUp->ThrustStrength = HoverThrusterStrength;
		}else {}
		HoverToUp.Broadcast(1.f);
	}
	if (bHoverDownIsStuckActive) {
		if (bThrusterBased) {
			HoverDown->ThrustStrength = HoverThrusterStrength;
		}else {}
		HoverToDown.Broadcast(1.f);
	}

	if (str.Equals("0.0")) {
		if (!bHoverUpIsIncapacitated && !bHoverUpIsStuckActive) {
			if (bThrusterBased) {
				HoverUp->ThrustStrength = HoverThrusterStrength;
			}
			else {}
			HoverToUp.Broadcast(AxisValue);
		}

		if (!bHoverDownIsStuckActive && bThrusterBased)
			HoverDown->ThrustStrength = 0.f;
	
	}
	else if (str.Equals("0.857143")) {

		if (!bHoverLeftIsIncapacitated && !bHoverLeftIsStuckActive) {
			if (bThrusterBased) {
				HoverLeft->ThrustStrength = HoverThrusterStrength;
			}
			HoverToLeft.Broadcast(AxisValue);
		}

		if(!bHoverRightIsStuckActive && bThrusterBased)
			HoverRight->ThrustStrength = 0.f;
	}
	else if (str.Equals("0.285714")) {
		if (!bHoverRightIsIncapacitated && !bHoverRightIsStuckActive) {
			if (bThrusterBased) {
				HoverRight->ThrustStrength = HoverThrusterStrength;
			}else {}
			HoverToRight.Broadcast(AxisValue);
		}

		if(!bHoverLeftIsStuckActive && bThrusterBased)
			HoverLeft->ThrustStrength = 0.f;	
	}
	else if (str.Equals("0.571429")) {
		if (!bHoverDownIsIncapacitated && !bHoverDownIsStuckActive) {
			if (bThrusterBased) {
				HoverDown->ThrustStrength = HoverThrusterStrength;
			}else {}
			HoverToDown.Broadcast(AxisValue);
		}

		if(!bHoverUpIsStuckActive && bThrusterBased)
			HoverUp->ThrustStrength = 0.f;			
	}
	else 
	{
		if(!bHoverUpIsStuckActive && bThrusterBased)
			HoverUp->ThrustStrength = 0.f;

		if(!bHoverLeftIsStuckActive && bThrusterBased)
			HoverLeft->ThrustStrength = 0.f;

		if(!bHoverRightIsStuckActive && bThrusterBased)
			HoverRight->ThrustStrength = 0.f;

		if(!bHoverDownIsStuckActive && bThrusterBased)
			HoverDown->ThrustStrength = 0.f;

		if(!bHoverDownIsStuckActive && !bHoverUpIsStuckActive && !bHoverRightIsStuckActive && !bHoverLeftIsStuckActive)
			HoverNeutral.Broadcast(AxisValue);
	}
}

void UPlayerInputController::Thrust(float AxisValue)
{
	ThrottleValue = AxisValue;

	if (!bMainEngineIsIncapacitated && !bMainEngineIsStuckActive) {
		if (bThrusterBased) {
			MainEngine->ThrustStrength = MainThrusterStrength * AxisValue;
			
		}else {}
		//MainThrust.Broadcast(AxisValue);
		ForwardVector = ForwardVector - (Owner->GetActorForwardVector() * (AxisValue * -1.f) * GetWorld()->DeltaTimeSeconds);
	}
	else if (bMainEngineIsStuckActive) {
		if (bThrusterBased) {
			MainEngine->ThrustStrength = MainThrusterStrength;
		}else {}
		MainThrust.Broadcast(1.f);
	}
}

float UPlayerInputController::GetThrottleValue()
{
	return ThrottleValue;
}

void UPlayerInputController::GetControlInfo(float & PitchOUT, float & RollOUT, float & YawOUT, float & ThrottleOUT)
{
	PitchOUT = PitchValue;
	RollOUT = RollValue;
	YawOUT = YawValue;
	ThrottleOUT = ThrottleValue;
}

void UPlayerInputController::SetComponentDamage(UPhysicsThrusterComponent * ThrusterHit, bool IsIncapacitated, bool IsStuckActive)
{
	FString Thruster = ThrusterHit->GetName();
	//UE_LOG(LogTemp, Error, TEXT("%s is hit"), *Thruster);
	//TODO thruster switch
	if (Thruster.Equals(MainEngine->GetName())) {
		bMainEngineIsIncapacitated = IsIncapacitated;
		bMainEngineIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(HoverLeft->GetName())) {
		
		bHoverLeftIsIncapacitated = IsIncapacitated;
		bHoverLeftIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(HoverDown->GetName())) {
		bHoverDownIsIncapacitated = IsIncapacitated;
		bHoverDownIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(HoverUp->GetName())) {
		bHoverUpIsIncapacitated = IsIncapacitated;
		bHoverUpIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(HoverRight->GetName())) {
		bHoverRightIsIncapacitated = IsIncapacitated;
		bHoverRightIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(RollLeft->GetName())) {
		bRollLeftIsIncapacitated = IsIncapacitated;
		bRollLeftIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(RollRight->GetName())) {
		bRollRightIsIncapacitated = IsIncapacitated;
		bRollRightIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(RollLeftUpper->GetName())) {
		bRollLeftUpperIsIncapacitated = IsIncapacitated;
		bRollLeftUpperIsStuckActive = IsStuckActive;
	}
	else if (Thruster.Equals(RollRightUpper->GetName())) {
		bRollRightUpperIsIncapacitated = IsIncapacitated;
		bRollRightUpperIsStuckActive = IsStuckActive;
	}
}



void UPlayerInputController::Fire()
{
	Owner->Fire();
}

void UPlayerInputController::SetInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if(InputComponent) {
		InputComponent->BindAxis("Pitch", this, &UPlayerInputController::JoystickPitch);
		InputComponent->BindAxis("FlyByPitch", this, &UPlayerInputController::FlyByPitch);
		InputComponent->BindAxis("Roll", this, &UPlayerInputController::JoystickRoll);
		InputComponent->BindAxis("FlyByRoll", this, &UPlayerInputController::FlyByRoll);
		InputComponent->BindAxis("Yaw", this, &UPlayerInputController::JoystickYaw);
		InputComponent->BindAxis("FlyByYaw", this, &UPlayerInputController::FlyByYaw);
		InputComponent->BindAxis("Hover", this, &UPlayerInputController::JoystickHover);
		InputComponent->BindAxis("Thrust", this, &UPlayerInputController::JoystickThrust);
		InputComponent->BindAxis("FlyByThrust", this, &UPlayerInputController::FlyByThrust);
		InputComponent->BindAction("Fire", IE_Pressed, this, &UPlayerInputController::Fire);
		InputComponent->BindAction("ToggleFlightMode", IE_Pressed, this, &UPlayerInputController::ToggleFlightMode);
		InputComponent->BindAction("Menu", IE_Pressed, this, &UPlayerInputController::ExitGame);
	}
}

void UPlayerInputController::JoystickPitch(float AxisValue)
{
	if (ControlMethod == EControlMethod::Joystick)
		Pitch(AxisValue);
}

void UPlayerInputController::JoystickRoll(float AxisValue)
{
	if (ControlMethod == EControlMethod::Joystick)
		Roll(AxisValue);
}

void UPlayerInputController::JoystickYaw(float AxisValue)
{
	if (ControlMethod == EControlMethod::Joystick)
		Yaw(AxisValue);
}

void UPlayerInputController::JoystickThrust(float AxisValue)
{
	if (ControlMethod == EControlMethod::Joystick)
		Thrust(AxisValue);
}

void UPlayerInputController::JoystickHover(float AxisValue)
{
	if (ControlMethod == EControlMethod::Joystick)
		Hover(AxisValue);
}

void UPlayerInputController::FlyByPitch(float AxisValue) {
	if (ControlMethod == EControlMethod::Gamepad) {
		if (AxisValue > 0.f)
		{
			Pitch(AxisValue / 2 + 0.5f);
		}
		else if (AxisValue == 0.f) {
			Pitch(0.5f);
		}
		else {
			Pitch(0.5f + AxisValue / 2);
		}
	}
}

void UPlayerInputController::FlyByRoll(float AxisValue) {
	if (ControlMethod == EControlMethod::Gamepad) {
		if (AxisValue > 0.f)
		{
			Roll(AxisValue / 2 + 0.5f);
		}
		else if (AxisValue == 0.f) {
			Roll(0.5f);
		}
		else {
			Roll(0.5f + AxisValue / 2);
		}
	}
}

void UPlayerInputController::FlyByYaw(float AxisValue) {
	if (ControlMethod == EControlMethod::Gamepad) {
		if (AxisValue > 0.f)
		{
			Yaw(AxisValue / 2 + 0.5f);
		}
		else if (AxisValue == 0.f) {
			Yaw(0.5f);
		}
		else {
			Yaw(0.5f + AxisValue / 2);
		}
	}
}

void UPlayerInputController::FlyByThrust(float AxisValue) {

	if (ControlMethod == EControlMethod::Gamepad)
		Thrust(FMath::Clamp(AxisValue, 0.f, 1.f));
}

void UPlayerInputController::SetThrusters()
{
	TArray<UActorComponent*> temp = GetOwner()->GetComponentsByClass(UPhysicsThrusterComponent::StaticClass());
	//UE_LOG(LogTemp, Error, TEXT("%d thrusters detected"), temp.Num());
	for (int i = 0; i < temp.Num(); i++) {
		if (temp[i]->GetName().Contains("MainEngine")) {
			MainEngine = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("NosePitchDown")) {
			NoseDown = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("NosePitchUp")) {
			NoseUp = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("ShipRollLeft")) {
			RollLeft = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("ShipRollLeftUpper")) {
			RollLeftUpper = Cast<UPhysicsThrusterComponent>(temp[i]);
			//UE_LOG(LogTemp, Error, TEXT("Found it!"));
		}
		else if (temp[i]->GetName().Equals("ShipRollRight")) {
			RollRight = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("ShipRollRightUpper")) {
			RollRightUpper = Cast<UPhysicsThrusterComponent>(temp[i]);
			//UE_LOG(LogTemp, Error, TEXT("Found it!"));
		}
		else if (temp[i]->GetName().Equals("ShipYawLeft")) {
			YawLeft = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("ShipYawRight")) {
			YawRight = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("SideHoverLeft")) {
			HoverLeft = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("SideHoverRight")) {
			HoverRight = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("HoverUp")) {
			HoverUp = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("HoverDown")) {
			HoverDown = Cast<UPhysicsThrusterComponent>(temp[i]);
		}
		else if (temp[i]->GetName().Equals("NosePitchUpAft")) {
			NoseUpAft = Cast<UPhysicsThrusterComponent>(temp[i]);
			UE_LOG(LogTemp, Error, TEXT("NoseUpAft found!"));
		}
		else if (temp[i]->GetName().Equals("NosePitchDownAft")) {
			NoseDownAft = Cast<UPhysicsThrusterComponent>(temp[i]);
			UE_LOG(LogTemp, Error, TEXT("NoseDownAft found!"));
		}
	}
}

void UPlayerInputController::SetWeaponController()
{
	WeaponController = GetOwner()->FindComponentByClass<UShipWeaponController>();
}

void UPlayerInputController::ToggleFlightMode()
{
	switch (ControlMethod)
	{
	case EControlMethod::Gamepad:
		ControlMethod = EControlMethod::Joystick;
		break;
	case EControlMethod::Joystick:
		ControlMethod = EControlMethod::Gamepad;
		break;
	case EControlMethod::Keyboard:
		break;
	default:
		break;
	}
}

void UPlayerInputController::ExitGame()
{

	UKismetSystemLibrary::QuitGame(GetWorld(), GetWorld()->GetFirstPlayerController() , EQuitPreference::Quit, false);
}

void UPlayerInputController::CompileControlValues()
{

	FRotator CurrentRotation(PitchValue, YawValue, RollValue);
	FQuat Change = EulerToQuaternion(CurrentRotation);
	AddLocalRotationQuat(Change);
}

void UPlayerInputController::CalculateSpeed()
{
	FVector Vel = GetOwner()->GetVelocity();
	FVector Direction;
	Vel.ToDirectionAndLength(Direction, Speed);
	//UE_LOG(LogTemp, Warning, TEXT("Speed: %f"), Speed);
	if (Speed > 3000.f) {
		SetLinearDamping(false);
	}
	else if (bDampingEnabled) {
		SetLinearDamping(true);
	}
}

void UPlayerInputController::SetLinearDamping(bool DisableDampening)
{
	if ((GetOwner())) {
		auto Owner = Cast<APlayerShip>(GetOwner());
		auto ShipMesh = Owner->GetShipReference();
		
		if (DisableDampening) {
			ShipMesh->SetLinearDamping(0);
			bDampingEnabled = false;
		}
		else {
			ShipMesh->SetLinearDamping((Speed - 3000));
			bDampingEnabled = true;
		}
	}
}

// Formula to convert a Euler angle in degrees to a quaternion rotation
FQuat UPlayerInputController::EulerToQuaternion(FRotator Current_Rotation)
{
	FQuat q;                                            // Declare output quaternion
	float yaw = Current_Rotation.Yaw * PI / 180;        // Convert degrees to radians
	float roll = Current_Rotation.Roll * PI / 180;
	float pitch = Current_Rotation.Pitch * PI / 180;

	double cy = cos(yaw * 0.5);
	double sy = sin(yaw * 0.5);
	double cr = cos(roll * 0.5);
	double sr = sin(roll * 0.5);
	double cp = cos(pitch * 0.5);
	double sp = sin(pitch * 0.5);

	q.W = cy * cr * cp + sy * sr * sp;
	q.X = cy * sr * cp - sy * cr * sp;
	q.Y = cy * cr * sp + sy * sr * cp;
	q.Z = sy * cr * cp - cy * sr * sp;

	return q;                                           // Return the quaternion of the input Euler rotation
}

// Add the input delta rotation to the scene component's current local rotation
void UPlayerInputController::AddLocalRotationQuat(const FQuat& Delta_Rotation)
{
	FHitResult RotationSweepHitResult;

	FVector Velocity = GetOwner()->GetVelocity();
	
	Velocity.ToDirectionAndLength(
		VelocityDirection,
		VelocityLength
	);

	Owner->AddActorLocalRotation(Delta_Rotation);
}