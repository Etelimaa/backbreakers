// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerShipController.h"

void APlayerShipController::BeginPlay()
{
	Super::BeginPlay();
	auto PlayerShip = GetControlledShip();

	if (PlayerShip)
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerShipController Possessing Ship: %s"), *PlayerShip->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerShipController cannot find ship!"));
	}
}

void APlayerShipController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

APlayerShip * APlayerShipController::GetControlledShip() const
{
	return Cast<APlayerShip>(GetWorld()->GetFirstPlayerController()->GetPawn());
}