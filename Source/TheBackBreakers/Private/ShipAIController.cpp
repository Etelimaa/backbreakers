// Fill out your copyright notice in the Description page of Project Settings.

#include "ShipAIController.h"
#include "PlayerShip.h"
#include "PlayerInputController.h"
#include "Engine/World.h"

void AShipAIController::BeginPlay()
{
	Super::BeginPlay();

	APlayerShip* ControlledShip = GetControlledShip();
	if (ControlledShip) {
		UE_LOG(LogTemp, Warning, TEXT("ShipAIController Possessing Ship: %s"), *ControlledShip->GetName());
	}
	else
		UE_LOG(LogTemp, Error, TEXT("ShipAIController Ship Not Found!"));
}

void AShipAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	GetControlledShip()->Pitch();
	GetControlledShip()->Roll();
	GetControlledShip()->Yaw();
	GetControlledShip()->Hover();
	GetControlledShip()->Thrust();
}

APlayerShip * AShipAIController::GetControlledShip() const
{
	return Cast<APlayerShip>(GetPawn());
}
