// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerShip.h"
#include "PlayerInputController.h"
#include "ShipWeaponController.h"
#include "Weapon_AC.h"
#include "Projectile.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "PhysicsEngine/PhysicsThrusterComponent.h"
#include "GameFramework/Actor.h"

// Sets default values
APlayerShip::APlayerShip()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponController = CreateDefaultSubobject<UShipWeaponController>(FName("Weapon Controller"));
}

void APlayerShip::SetACReference(UWeapon_AC * WeaponToSet_Right, UWeapon_AC * WeaponToSet_Left)
{
	WeaponController->SetAutocannons(WeaponToSet_Right, WeaponToSet_Left);
	AC_Right = WeaponToSet_Right;
	AC_Left = WeaponToSet_Left;
}

void APlayerShip::SetShipReference(UStaticMeshComponent * Ship)
{
	WeaponController->SetParentShip(Ship);
	ShipMesh = Ship;
}

UStaticMeshComponent * APlayerShip::GetShipReference()
{
	return ShipMesh;
}

UPlayerInputController* APlayerShip::GetShipInputComponent()
{
	return PlayerInputController;
}

void APlayerShip::SetCustomPlayerInputController(UPlayerInputController * InputController)
{
	PlayerInputController = InputController;
}

// Called when the game starts or when spawned
void APlayerShip::BeginPlay()
{
	Super::BeginPlay();
	WeaponController->AlignWeaponsToConvergence();
	LastPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
}

// Called every frame
void APlayerShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

float APlayerShip::GetWeaponConvergence()
{
	return WeaponConvergence;
}

void APlayerShip::Fire()
{
	//Spawn Projectiles
	FVector Location = AC_Right->GetSocketLocation(FName("Projectile"));
	FTransform ProjectileTransform = WeaponController->GetProjectileTransform(Location);

	GetWorld()->SpawnActor<AProjectile>(ToSpawn, ProjectileTransform);

	Location = AC_Left->GetSocketLocation(FName("Projectile"));
	ProjectileTransform = WeaponController->GetProjectileTransform(Location);

	GetWorld()->SpawnActor<AProjectile>(ToSpawn, ProjectileTransform);
}

FTransform APlayerShip::GetShipTransform()
{

	FVector Forward = GetOwner()->GetActorForwardVector();

	FVector Right = Forward;
	Right.Normalize();
	Right.CrossProduct(GetOwner()->GetActorUpVector(), Right);

	FVector Up = GetOwner()->GetActorUpVector();

	FRotator ShipRotation = UKismetMathLibrary::MakeRotationFromAxes(Forward, Right, Up);

	FTransform ShipTransform(ShipRotation, GetOwner()->GetActorLocation(), FVector(1, 1, 1));
	
	return ShipTransform;
}

void APlayerShip::Pitch()
{
	PlayerInputController->Pitch(0.5f);
}

void APlayerShip::Roll()
{
	PlayerInputController->Roll(0.5f);
}

void APlayerShip::Yaw()
{
	PlayerInputController->Yaw(0.5f);
}

void APlayerShip::Hover()
{
	PlayerInputController->Hover(1.f);
}

void APlayerShip::Thrust()
{
	PlayerInputController->Thrust(0.f);
}

FTransform APlayerShip::GetProjectileTransform(FVector SocketLocation)
{
	FTransform ProjectileTransform = WeaponController->GetProjectileTransform(SocketLocation);
	return ProjectileTransform;
}

void APlayerShip::SetComponentDamage(UPhysicsThrusterComponent* ThrusterHit)
{
	float mod = FMath::FRand();
	if (PlayerInputController) {
		if (mod > 0.9f) {
			PlayerInputController->SetComponentDamage(ThrusterHit, false, true);
			UE_LOG(LogTemp, Error, TEXT("STUCK!"));
		}
		else if (mod > 0.5) {
			PlayerInputController->SetComponentDamage(ThrusterHit, true, false);
			UE_LOG(LogTemp, Error, TEXT("Deactivated!"));
		}
		else {
			PlayerInputController->SetComponentDamage(ThrusterHit, false, false);
		}
	}
}

