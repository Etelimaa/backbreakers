// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileMovementController.h"
#include "Projectile.h"

// Sets default values for this component's properties
UProjectileMovementController::UProjectileMovementController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UProjectileMovementController::BeginPlay()
{
	Super::BeginPlay();

	Owner = Cast<AProjectile>(GetOwner());
}


// Called every frame
void UProjectileMovementController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
	FVector NewLocation = Owner->GetActorLocation() + Owner->GetActorRotation().Vector() * Owner->GetSpeed() * DeltaTime;

	Owner->SetActorLocation(NewLocation);
	
}

