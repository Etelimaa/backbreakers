// Fill out your copyright notice in the Description page of Project Settings.

#include "ShipWeaponController.h"
#include "Weapon_AC.h"
#include "Projectile.h"
#include "PlayerShip.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
UShipWeaponController::UShipWeaponController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UShipWeaponController::BeginPlay()
{
	Super::BeginPlay();

	// ...
	Owner = Cast<APlayerShip>(GetOwner());
}

void UShipWeaponController::SetParentShip(UStaticMeshComponent * Ship)
{
	ParentShip = Ship;

}

void UShipWeaponController::SetAutocannons(UWeapon_AC * Right, UWeapon_AC * Left)
{
	AC_Right = Right;
	AC_Left = Left;
}

void UShipWeaponController::AlignWeaponsToConvergence()
{
	SetWeaponConvergence();
	FVector AC_RightLocation = ParentShip->GetSocketLocation(FName("AC_Right"));
	FVector AC_LeftLocation = ParentShip->GetSocketLocation(FName("AC_Left"));
	FVector ConvergenceLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().Vector() * WeaponConvergence;

	FHitResult HitResult;
	const FName TraceTag("MyTraceTag");

	GetWorld()->DebugDrawTraceTag = TraceTag;

	FCollisionQueryParams CollisionParams;
	CollisionParams.TraceTag = TraceTag;


	DoAlignment();
	
}


// Called every frame
void UShipWeaponController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

FTransform UShipWeaponController::GetProjectileTransform(FVector SocketLocation)
{
	//Create transform for spawned projectile
	FVector ConvergencePoint = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().Vector() * WeaponConvergence;

	FVector Forward = ConvergencePoint - SocketLocation;

	FVector Right = Forward;
	Right.Normalize();
	Right.CrossProduct(GetOwner()->GetActorUpVector(), Right);
	
	FVector Up = GetOwner()->GetActorUpVector();

	FRotator ProjectileRotation = UKismetMathLibrary::MakeRotationFromAxes(Forward, Right, Up);

	FTransform ProjectileTransform(ProjectileRotation, SocketLocation, FVector(1, 1, 1));
	return ProjectileTransform;
	
}

void UShipWeaponController::DoAlignment()
{
	FVector AC_RightLocation = ParentShip->GetSocketLocation(FName("AC_Right"));
	FVector AC_LeftLocation = ParentShip->GetSocketLocation(FName("AC_Left"));
	FRotator AC_RightRotation = ParentShip->GetSocketRotation(FName("AC_Right"));
	FRotator AC_LeftRotation = ParentShip->GetSocketRotation(FName("AC_Left"));
	FVector ConvergenceLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().Vector() * WeaponConvergence;
	FVector ToConvergence = AC_RightLocation + ConvergenceLocation;
	ToConvergence.Normalize();

	FRotator NewRotation = ToConvergence.Rotation();
	AC_RightRotation.Yaw = 90 - NewRotation.Yaw;

	AC_Right->SetWorldRotation(AC_RightRotation);
	
	AC_LeftRotation.Yaw = 90 + NewRotation.Yaw;
	AC_Left->SetRelativeRotation(AC_LeftRotation);
}

void UShipWeaponController::SetWeaponConvergence()
{
	WeaponConvergence = Owner->GetWeaponConvergence();
}

