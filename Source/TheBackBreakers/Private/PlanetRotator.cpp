// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetRotator.h"

// Sets default values for this component's properties
UPlanetRotator::UPlanetRotator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UPlanetRotator::SetRotationParameters(bool Rotate, bool Randomize, float SpeedX, float SpeedY, float SpeedZ)
{
	bRotateBody = Rotate;
	if (Randomize) {
		RotationSpeedX = FMath::RandRange(0.01f, 0.25f);
		RotationSpeedY = FMath::RandRange(0.01f, 0.25f);
		RotationSpeedZ = FMath::RandRange(0.01f, 0.25f);
	}
	else {
		if (SpeedX != 0) { RotationSpeedX = SpeedX; }
		if (SpeedY != 0) { RotationSpeedY = SpeedY; }
		if (SpeedZ != 0) { RotationSpeedZ = SpeedZ; }
	}
}


// Called when the game starts
void UPlanetRotator::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlanetRotator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bRotateBody) {
		FRotator CurrentRotation(RotationSpeedX, RotationSpeedY, RotationSpeedZ);
		FQuat DeltaRotation = EulerToQuaternion(CurrentRotation);
		AddLocalRotationQuat(DeltaRotation);
	}
}

// Formula to convert a Euler angle in degrees to a quaternion rotation
FQuat UPlanetRotator::EulerToQuaternion(FRotator Current_Rotation)
{
	FQuat q;                                            // Declare output quaternion
	float yaw = Current_Rotation.Yaw * PI / 180;        // Convert degrees to radians
	float roll = Current_Rotation.Roll * PI / 180;
	float pitch = Current_Rotation.Pitch * PI / 180;

	double cy = cos(yaw * 0.5);
	double sy = sin(yaw * 0.5);
	double cr = cos(roll * 0.5);
	double sr = sin(roll * 0.5);
	double cp = cos(pitch * 0.5);
	double sp = sin(pitch * 0.5);

	q.W = cy * cr * cp + sy * sr * sp;
	q.X = cy * sr * cp - sy * cr * sp;
	q.Y = cy * cr * sp + sy * sr * cp;
	q.Z = sy * cr * cp - cy * sr * sp;

	return q;                                           // Return the quaternion of the input Euler rotation
}

// Add the input delta rotation to the scene component's current local rotation
void UPlanetRotator::AddLocalRotationQuat(const FQuat& Delta_Rotation)
{
	GetOwner()->AddActorLocalRotation(Delta_Rotation);
}

