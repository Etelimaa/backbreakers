// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheBackBreakersGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THEBACKBREAKERS_API ATheBackBreakersGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
