// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "PlayerShip.h"
#include "PlayerShipController.generated.h"

/**
 * 
 */
UCLASS()
class THEBACKBREAKERS_API APlayerShipController : public APlayerController
{
	GENERATED_BODY()
	
private:

	//Methods
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	APlayerShip* GetControlledShip() const;
};
