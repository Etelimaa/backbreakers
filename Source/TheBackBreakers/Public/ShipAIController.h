// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "PlayerShip.h"
#include "ShipAIController.generated.h"

/**
 * 
 */
UCLASS()
class THEBACKBREAKERS_API AShipAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime)  override;

private:

	APlayerShip * GetControlledShip() const;
};
