// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PlayerInputController.generated.h"

// Enum for control method
UENUM()
enum class EControlMethod : uint8
{
	Gamepad,
	Joystick,
	Keyboard
};

class UPhysicsThrusterComponent;
class UAudioComponent;
class UShipWeaponController;
class UWeapon_AC;
class APlayerShip;
/*
* Controls Movement and input of Ship Pawns
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomInputEvent, float, AxisValue);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEBACKBREAKERS_API UPlayerInputController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInputController();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Thrusters------------------------------

	UPROPERTY(EditAnywhere, category = "Thrusters")
		float PitchThrusterStrength = 200000.f;

	UPROPERTY(EditAnywhere, category = "Thrusters")
		float RollThrusterStrength = 200000.f;

	UPROPERTY(EditAnywhere, category = "Thrusters")
		float YawThrusterStrength = 200000.f;

	UPROPERTY(EditAnywhere, category = "Thrusters")
		float HoverThrusterStrength = 150000.f;

	UPROPERTY(EditAnywhere, category = "Thrusters")
		float MainThrusterStrength = 200000.f;

	//Events-------------------------

	//Pitch
	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent PitchUp;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent PitchDown;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent PitchNeutral;

	//Roll
	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent RollToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent RollToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent RollNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent YawToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent YawToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent YawNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent HoverToUp;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent HoverToDown;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent HoverToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent HoverToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent HoverNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomInputEvent MainThrust;

	void SetComponentDamage(UPhysicsThrusterComponent* ThrusterHit, bool IsIncapacitated, bool IsStuckActive);

	//Ship movement
	void Pitch(float AxisValue);
	void Roll(float AxisValue);
	void Yaw(float AxisValue);
	void Hover(float AxisValue);
	void Thrust(float AxisValue);
	float GetThrottleValue();

	float PitchValue = 0.f;
	float RollValue = 0.f;
	float YawValue = 0.f;
	float ThrottleValue = 0.f;

	FVector ForwardVelocity;
	FVector ForwardVector;
	FVector VelocityDirection;
	float VelocityLength;
	FVector LastForwardVector;

	//UI Info

	UFUNCTION(BlueprintCallable, Category = "User Interface")
	void GetControlInfo(float & PitchOUT, float & RollOut, float & YawOut, float & ThrottleOUT);

private:

	//Thruster issues

	bool bNoseUpIsIncapacitated = false;
	bool bNoseUpIsStuckActive = false;

	bool bNoseDownIsIncapacitated = false;
	bool bNoseDownIsStuckActive = false;

	bool bRollLeftIsIncapacitated = false;
	bool bRollLeftIsStuckActive = false;

	bool bRollLeftUpperIsIncapacitated = false;
	bool bRollLeftUpperIsStuckActive = false;

	bool bRollRightIsIncapacitated = false;
	bool bRollRightIsStuckActive = false;

	bool bRollRightUpperIsIncapacitated = false;
	bool bRollRightUpperIsStuckActive = false;

	bool bYawLeftIsIncapacitated = false;
	bool bYawLeftIsStuckActive = false;

	bool bYawRightIsIncapacitated = false;
	bool bYawRightIsStuckActive = false;

	bool bHoverLeftIsIncapacitated = false;
	bool bHoverLeftIsStuckActive = false;

	bool bHoverRightIsIncapacitated = false;
	bool bHoverRightIsStuckActive = false;

	bool bHoverUpIsIncapacitated = false;
	bool bHoverUpIsStuckActive = false;

	bool bHoverDownIsIncapacitated = false;
	bool bHoverDownIsStuckActive = false;

	bool bMainEngineIsIncapacitated = false;
	bool bMainEngineIsStuckActive = false;

	//Weapons

	void Fire();

	void SetInputComponent();
	void SetThrusters();
	void SetWeaponController();

	UShipWeaponController* WeaponController = nullptr;

	UPhysicsThrusterComponent* NoseUp = nullptr;
	UPhysicsThrusterComponent* NoseDown = nullptr;
	UPhysicsThrusterComponent* NoseUpAft = nullptr;
	UPhysicsThrusterComponent* NoseDownAft = nullptr;
	UPhysicsThrusterComponent* RollLeft = nullptr;
	UPhysicsThrusterComponent* RollLeftUpper = nullptr;
	UPhysicsThrusterComponent* RollRight = nullptr;
	UPhysicsThrusterComponent* RollRightUpper = nullptr;
	UPhysicsThrusterComponent* YawLeft = nullptr;
	UPhysicsThrusterComponent* YawRight = nullptr;
	UPhysicsThrusterComponent* HoverLeft = nullptr;
	UPhysicsThrusterComponent* HoverRight = nullptr;
	UPhysicsThrusterComponent* HoverUp = nullptr;
	UPhysicsThrusterComponent* HoverDown = nullptr;
	UPhysicsThrusterComponent* MainEngine = nullptr;

	UInputComponent* InputComponent = nullptr;
	APlayerShip* Owner = nullptr;

	// Control method

	bool bThrusterBased = true;
	EControlMethod ControlMethod = EControlMethod::Gamepad;

	void ToggleFlightMode();

	// Ship movement input

	// Joystick input sent straight to input, flyby input needs own calculations to be compatible
	void JoystickPitch(float AxisValue);
	void JoystickRoll(float AxisValue);
	void JoystickYaw(float AxisValue);
	void JoystickThrust(float AxisValue);
	void JoystickHover(float AxisValue);

	void FlyByPitch(float AxisValue);
	void FlyByRoll(float AxisValue);
	void FlyByYaw(float AxisValue);
	void FlyByThrust(float AxisValue);

	// Utilities

	void ExitGame(); // TODO For now menu button calls this

	//Telemetrics

	void CalculateSpeed();
	void SetLinearDamping(bool DisableDampening);
	bool bDampingEnabled = false;
	float Speed = 0.f;
	float Timer = 0.f;

	//math

	void CompileControlValues();
	FQuat EulerToQuaternion(FRotator Current_Rotation);
	void AddLocalRotationQuat(const FQuat & Delta_Rotation);
};
