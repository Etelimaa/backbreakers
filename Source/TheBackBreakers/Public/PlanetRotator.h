// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlanetRotator.generated.h"

/*
* For rotating planetary bodies
*/

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEBACKBREAKERS_API UPlanetRotator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlanetRotator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FQuat EulerToQuaternion(FRotator Current_Rotation);

	void AddLocalRotationQuat(const FQuat & Delta_Rotation);

	UFUNCTION(BlueprintCallable, category = "Rotation Setup")
	void SetRotationParameters(bool Rotate, bool Randomize, float SpeedX, float SpeedY, float SpeedZ);

	//Rotate the planetary body?
	UPROPERTY(EditAnywhere, category = "Planetary")
	bool bRotateBody = false;
	//Rotationspeed of the body
	UPROPERTY(EditAnywhere, category = "Planetary")
	float RotationSpeedX = 0.f;
	//Rotationspeed of the body
	UPROPERTY(EditAnywhere, category = "Planetary")
	float RotationSpeedY = 0.f;
	//Rotationspeed of the body
	UPROPERTY(EditAnywhere, category = "Planetary")
	float RotationSpeedZ = 0.1f;
		
};
