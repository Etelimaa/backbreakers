// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerShip.generated.h"

class UWeapon_AC;
class UPlayerInputController;
class UShipWeaponController;
class UStaticMeshComponent;
class UPhysicsThrusterComponent;

UCLASS()
class THEBACKBREAKERS_API APlayerShip : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerShip();

	UFUNCTION(BlueprintCallable, Category = ShipSetup)
	void SetACReference(UWeapon_AC* WeaponToSet_Right, UWeapon_AC* WeaponToSet_Left);

	UFUNCTION(BlueprintCallable, Category = ShipSetup)
	void SetShipReference(UStaticMeshComponent* Ship);

	UStaticMeshComponent* GetShipReference();

	UFUNCTION(BlueprintCallable, Category = ShipSetup)
	UPlayerInputController* GetShipInputComponent();

	UFUNCTION(BlueprintCallable, Category = ShipSetup)
	void SetCustomPlayerInputController(UPlayerInputController* InputController);

	UFUNCTION(BlueprintCallable, Category = ShipSetup)
	FTransform GetProjectileTransform(FVector SocketLocation);

	UFUNCTION(BlueprintCallable, Category = ComponentDamage)
	void SetComponentDamage(UPhysicsThrusterComponent* ThrusterHit);

	UPROPERTY(EditAnywhere, category = "Munitions")
	TSubclassOf<class AProjectile> ToSpawn;

	UPROPERTY(EditAnywhere, category = "Weapons")
	float WeaponConvergence = 2000.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float GetWeaponConvergence();

	void Fire();

	FTransform GetShipTransform();

	// For AI movement

	void Pitch();
	void Roll();
	void Yaw();
	void Hover();
	void Thrust();

private:

	float Speed;
	FVector LastPosition;
	FVector CurrentPosition;
	UPlayerInputController* PlayerInputController = nullptr;
	UShipWeaponController* WeaponController = nullptr;
	UWeapon_AC* AC_Right = nullptr;
	UWeapon_AC* AC_Left = nullptr;
	UStaticMeshComponent* ShipMesh = nullptr;
};
