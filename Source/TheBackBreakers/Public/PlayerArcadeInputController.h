// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PlayerArcadeInputController.generated.h"

class UPhysicsThrusterComponent;
class UAudioComponent;
class UShipWeaponController;
class UWeapon_AC;
class APlayerShip;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomArcadeInputEvent, float, AxisValue);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEBACKBREAKERS_API UPlayerArcadeInputController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerArcadeInputController();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Pitch
	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent PitchUp;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent PitchDown;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent PitchNeutral;

	//Roll
	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent RollToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent RollToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent RollNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent YawToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent YawToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent YawNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent HoverToUp;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent HoverToDown;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent HoverToRight;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent HoverToLeft;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent HoverNeutral;

	UPROPERTY(BlueprintAssignable)
		FCustomArcadeInputEvent MainThrust;

	void SetComponentDamage(UPhysicsThrusterComponent* ThrusterHit, bool IsIncapacitated, bool IsStuckActive);

	//Ship movement
	void Pitch(float AxisValue);
	void Roll(float AxisValue);
	void Yaw(float AxisValue);
	void Hover(float AxisValue);
	void Thrust(float AxisValue);

private:

	//Thruster issues

	bool bNoseUpIsIncapacitated = false;
	bool bNoseUpIsStuckActive = false;

	bool bNoseDownIsIncapacitated = false;
	bool bNoseDownIsStuckActive = false;

	bool bRollLeftIsIncapacitated = false;
	bool bRollLeftIsStuckActive = false;

	bool bRollLeftUpperIsIncapacitated = false;
	bool bRollLeftUpperIsStuckActive = false;

	bool bRollRightIsIncapacitated = false;
	bool bRollRightIsStuckActive = false;

	bool bRollRightUpperIsIncapacitated = false;
	bool bRollRightUpperIsStuckActive = false;

	bool bYawLeftIsIncapacitated = false;
	bool bYawLeftIsStuckActive = false;

	bool bYawRightIsIncapacitated = false;
	bool bYawRightIsStuckActive = false;

	bool bHoverLeftIsIncapacitated = false;
	bool bHoverLeftIsStuckActive = false;

	bool bHoverRightIsIncapacitated = false;
	bool bHoverRightIsStuckActive = false;

	bool bHoverUpIsIncapacitated = false;
	bool bHoverUpIsStuckActive = false;

	bool bHoverDownIsIncapacitated = false;
	bool bHoverDownIsStuckActive = false;

	bool bMainEngineIsIncapacitated = false;
	bool bMainEngineIsStuckActive = false;

	//Weapons

	void Fire();

	void SetInputComponent();
	void SetThrusters();
	void SetWeaponController();

	UShipWeaponController* WeaponController = nullptr;

	UInputComponent* InputComponent = nullptr;
	APlayerShip* Owner = nullptr;
};
