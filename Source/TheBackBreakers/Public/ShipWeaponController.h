// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ShipWeaponController.generated.h"

class UWeapon_AC;
class UStaticMeshComponent;
class AProjectile;
class APlayerShip;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCustomSpawnEvent, FTransform, Transform);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEBACKBREAKERS_API UShipWeaponController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UShipWeaponController();

	UPROPERTY(BlueprintAssignable)
		FCustomSpawnEvent SpawnProjectile;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetParentShip(UStaticMeshComponent* Ship);
	void SetAutocannons(UWeapon_AC * Right, UWeapon_AC * Left);
	void AlignWeaponsToConvergence();
	FTransform GetProjectileTransform(FVector SocketLocation);

private:

	void DoAlignment();
	void SetWeaponConvergence();

	float WeaponConvergence = 2000.f;
	UStaticMeshComponent* ParentShip = nullptr;
	UWeapon_AC* AC_Right = nullptr;
	UWeapon_AC* AC_Left = nullptr;
	APlayerShip* Owner = nullptr;
};
